FROM debian:stretch-slim

RUN apt-get update
RUN apt-get install -y software-properties-common gnupg2 apt-transport-https apt-utils
    
RUN echo "deb https://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
RUN echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee /etc/apt/sources.list.d/webupd8team-java.list
RUN echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886

#RUN add-apt-repository -y ppa:webupd8team/java
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections


RUN apt-get update --force-yes
RUN mkdir -p /usr/share/man/man1
RUN apt-get install -y --allow-unauthenticated oracle-java8-installer
RUN apt-get install oracle-java8-set-default
RUN apt-get install -y scala sbt

RUN apt-get install curl -y
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs build-essential


EXPOSE 9000
EXPOSE 3000

RUN mkdir /home/shared
#VOLUME ["/home/shared/"]
RUN echo "Lazy"

WORKDIR /home/shared
RUN git clone https://najt@bitbucket.org/x909/scala-nightmare.git 
WORKDIR scala-nightmare

WORKDIR /home/shared
RUN git clone https://najt@bitbucket.org/x909/react-areukiddingme.git
WORKDIR react-areukiddingme
RUN npm install 


COPY scripts/* /root/script/
CMD bash /root/script/run_scala_npm.sh

