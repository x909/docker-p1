name := """first app"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)


scalaVersion := "2.12.4"


libraryDependencies += guice
libraryDependencies += jdbc
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.+" % Test
libraryDependencies += "com.typesafe.slick" %% "slick" % "3.2.+"
libraryDependencies += "mysql" % "mysql-connector-java" % "5.+"
libraryDependencies += "com.typesafe.play" %% "anorm" % "2.5.+"

